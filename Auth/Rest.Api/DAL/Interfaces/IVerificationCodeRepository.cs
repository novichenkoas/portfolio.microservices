﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels.Base;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Interfaces;

public interface IVerificationCodeRepository<TEntity>
    where TEntity : VerificationCode
{
    public Task<DatabaseResult<TEntity>> GetAsync(string findParameter, CancellationToken cancellationToken);
    public Task<DatabaseResult<long>> CreateAsync(TEntity entity, CancellationToken cancellationToken);
    public Task<DatabaseResult<long>> UpdateAsync(TEntity entity, CancellationToken cancellationToken);
}