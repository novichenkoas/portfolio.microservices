﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbContext;

public class MainContext : IdentityDbContext<ApplicationUser, ApplicationRole, long>
{
    public MainContext(DbContextOptions<MainContext> options)
    : base(options)
    {

    }
}