﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels.Base;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels;
using Npgsql;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Repositories.Base;

public abstract class VerificationCodeBaseRepository<TEntity> : IVerificationCodeRepository<TEntity>
    where TEntity : VerificationCode
{
    protected readonly Microsoft.EntityFrameworkCore.DbContext _dataBaseContext;

    protected VerificationCodeBaseRepository(Microsoft.EntityFrameworkCore.DbContext mainContext) =>
        _dataBaseContext = mainContext;

    public async Task<DatabaseResult<TEntity>> GetAsync(string findParameter, CancellationToken cancellationToken)
    {
        try
        {
            TEntity verificationCode = await FindByParameter(findParameter, cancellationToken);
            if(verificationCode == null)
            {
                return DatabaseResultHelper.CreateError<TEntity>(DatabaseStatus.NotFound);
            }

            return DatabaseResultHelper.CreateSuccess(verificationCode);
        }
        catch(NpgsqlException ex)
        {
            return DatabaseResultHelper.CreateError<TEntity>(DatabaseStatus.DbError, ex.Message);
        }
        catch(Exception e)
        {
            return DatabaseResultHelper.CreateError<TEntity>(DatabaseStatus.GeneralError, e.Message);
        }
    }

    public async Task<DatabaseResult<long>> CreateAsync(TEntity entity, CancellationToken cancellationToken)
    {
        return await SaveAsync(entity, async () =>
        {
            await _dataBaseContext.AddAsync(entity, cancellationToken);
            await _dataBaseContext.SaveChangesAsync(cancellationToken);
        });
    }

    public async Task<DatabaseResult<long>> UpdateAsync(TEntity entity, CancellationToken cancellationToken)
    {
        return await SaveAsync(entity, async () =>
        {
            _dataBaseContext.Update(entity);
            await _dataBaseContext.SaveChangesAsync(cancellationToken);
        });
    }

    private static async Task<DatabaseResult<long>> SaveAsync(TEntity entity, Func<Task> dbSave)
    {
        try
        {
            await dbSave.Invoke();
        }
        catch(NpgsqlException ex)
        {
            return DatabaseResultHelper.CreateError<long>(DatabaseStatus.DbError, ex.Message);
        }
        catch(Exception e)
        {
            return DatabaseResultHelper.CreateError<long>(DatabaseStatus.GeneralError, e.Message);
        }

        return DatabaseResultHelper.CreateSuccess(entity.Id);
    }

    protected abstract Task<TEntity> FindByParameter(string findParameter, CancellationToken cancellationToken);
}
