﻿using Microsoft.EntityFrameworkCore;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels.Base;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Repositories.Base;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Repositories;

public class VerificationPhoneCodeRepository<TEntity> : VerificationCodeBaseRepository<TEntity>
    where TEntity : VerificationPhoneCode
{
    public VerificationPhoneCodeRepository(Microsoft.EntityFrameworkCore.DbContext mainContext)
        : base(mainContext)
    {
    }

    protected override async Task<TEntity> FindByParameter(string findParameter,
        CancellationToken cancellationToken) =>
        (await _dataBaseContext.Set<TEntity>()
            .FirstOrDefaultAsync(
                e => e.Phone == findParameter && !e.Used && !e.Canceled && e.Expire >= DateTime.UtcNow,
                cancellationToken))!;
}