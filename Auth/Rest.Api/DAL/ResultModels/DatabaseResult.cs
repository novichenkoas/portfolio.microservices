﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels.Enums;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels;

public class DatabaseResult<T>
{
    public DatabaseStatus Status { get; set; }

    public string Message { get; set; }

    public T Data { get; set; }

    public DatabaseResult(DatabaseStatus status, string message, T data)
    {
        Status = status;
        Message = message;
        Data = data;
    }
}