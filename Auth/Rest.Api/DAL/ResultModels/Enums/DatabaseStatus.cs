﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels.Enums;

public enum DatabaseStatus
{
    Success,
    NotFound,
    DbError,
    GeneralError
}