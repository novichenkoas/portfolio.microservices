﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels.Enums;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels;

public static class DatabaseResultHelper
{
    public static DatabaseResult<TModel> CreateError<TModel>(DatabaseStatus status, string message = "") =>
        new(status, message, data: default);

    public static DatabaseResult<TModel> CreateSuccess<TModel>(TModel data) =>
        new(DatabaseStatus.Success, string.Empty, data);
}