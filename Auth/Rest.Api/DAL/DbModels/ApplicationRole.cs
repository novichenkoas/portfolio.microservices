﻿using Microsoft.AspNetCore.Identity;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;

public class ApplicationRole: IdentityRole<long>
{
    
}