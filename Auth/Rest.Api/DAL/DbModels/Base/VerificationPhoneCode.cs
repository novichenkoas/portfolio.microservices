﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels.Base;

public class VerificationPhoneCode : VerificationCode
{
	public string Phone { get; set; }
}
