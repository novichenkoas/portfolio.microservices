﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels.Base;

public abstract class VerificationCode
{
    public long Id { get; set; }
    public DateTime Created { get; set; }
    public string Code { get; set; }
    public DateTime Expire { get; set; }
    public bool Used { get; set; }
    public bool Canceled { get; set; }
}
