﻿using Microsoft.AspNetCore.Identity;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;

public class ApplicationUser: IdentityUser<long>
{
    public long? ProfileId { get; set; }
}