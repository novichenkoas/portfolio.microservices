﻿using FluentValidation;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Enums;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Extensions;

public static class FluentValidatorExtension
{
	public static IRuleBuilderOptions<T, TProperty> WithMessage<T, TProperty>(
		this IRuleBuilderOptions<T, TProperty> rule, MessageKey messageKey) => rule.WithMessage(messageKey.ToString());

	public static IRuleBuilderOptions<T, string?> AsPasswordCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.NotNull().WithMessage(MessageKey.AuthPasswordIsNull)
			.NotEmpty().WithMessage(MessageKey.AuthPasswordIsEmpty)
			.MinimumLength(8).WithMessage(MessageKey.AuthPasswordIsShort)
			.Matches("[A-Z]").WithMessage(MessageKey.AuthPasswordIsNotContainUppercaseLetters)
			.Matches("[a-z]").WithMessage(MessageKey.AuthPasswordIsNotContainLowercaseLetters)
			.Matches("[0-9]").WithMessage(MessageKey.AuthPasswordIsNotContainNumbers)
			.Matches("[^a-zA-Z0-9]").WithMessage(MessageKey.AuthPasswordIsNotContainNonAlphabeticCharacters);

	public static IRuleBuilderOptions<T, string?> AsEmailCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.NotNull().WithMessage(MessageKey.AuthEmailIsNull)
			.NotEmpty().WithMessage(MessageKey.AuthEmailIsEmpty)
			.MaximumLength(ApplicationConstants.EmailMaxLengthLength).WithMessage(MessageKey.AuthEmailIsLong)
			.Matches(ApplicationConstants.EmailPattern).WithMessage(MessageKey.AuthEmailIsNotValid);

	public static IRuleBuilderOptions<T, string?> AsPhoneCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.NotNull().WithMessage(MessageKey.AuthPhoneIsNull)
			.NotEmpty().WithMessage(MessageKey.AuthPhoneIsEmpty)
			.Matches(ApplicationConstants.PhonePattern).WithMessage(MessageKey.AuthPhoneIsInvalid);

	public static IRuleBuilderOptions<T, string?> AsConfirmationCodeCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.NotNull().WithMessage(MessageKey.AuthCodeIsNull)
			.NotEmpty().WithMessage(MessageKey.AuthCodeIsEmpty)
			.Matches(ApplicationConstants.VerificationPhoneCodePattern).WithMessage(MessageKey.AuthCodeIsInvalid);

	private static IRuleBuilderOptions<T, string?> AsUserRoleCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.NotNull().WithMessage(MessageKey.AuthRoleIsNull)
			.NotEmpty().WithMessage(MessageKey.AuthRoleIsEmpty)
			.IsEnumName(typeof(UserRole)).WithMessage(MessageKey.AuthRoleIsNotExist);

	public static IRuleBuilderOptions<T, string?> AsUserClientRoleCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.AsUserRoleCustomRule()
			.Must(x => ApplicationConstants.ClientRoles.Contains(x)).WithMessage(MessageKey.AuthInvalidRoleToUse);

	public static IRuleBuilderOptions<T, string?> AsUserEmployeeRoleCustomRule<T>(
		this IRuleBuilderInitial<T, string?> rule) =>
		rule.AsUserRoleCustomRule()
			.Must(x => ApplicationConstants.EmployeeRoles.Contains(x)).WithMessage(MessageKey.AuthInvalidRoleToUse);

	public static IRuleBuilderOptions<T, long?> AsUserIdRule<T>(
		this IRuleBuilderInitial<T, long?> rule) =>
		rule.NotNull().WithMessage(MessageKey.AuthUserIdIsNull)
			.NotEmpty().WithMessage(MessageKey.AuthUserIdIsEmpty)
			.GreaterThan(0).WithMessage(MessageKey.AuthUserIdLessOrEqualZero)
			.LessThanOrEqualTo(long.MaxValue).WithMessage(MessageKey.AuthUserIdGreaterMaxValue);
}
