﻿using MediatR.Pipeline;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces
{
    public interface ICommandValidator<TModel, TResponce> : IRequestPreProcessor<TModel>
        where TModel : ICommand<TResponce>
    {
    }
}
