﻿using MediatR.Pipeline;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces
{
    public interface IQueryValidator<TModel, TResponse> : IRequestPreProcessor<TModel>
        where TModel : IQuery<TResponse>
    {
    }
}
