﻿using MediatR;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces
{
    public interface ICommandHandler<TCommand, TResponse> : IRequestHandler<TCommand, TResponse>
        where TCommand : ICommand<TResponse>
    {
    }
}
