﻿using MediatR;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces
{
    public interface ICommand<T> : IRequest<T>
    {

    }
}