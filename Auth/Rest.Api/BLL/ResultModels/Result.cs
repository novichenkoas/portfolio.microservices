﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;

public struct Result<TStatus, TData>
    where TStatus : Enum
{
    public TStatus Status { get; set; }

    public string Message { get; set; }

    public TData Data { get; set; }

    public Result(TStatus status)
    {
        Status = status;
        Data = default;
        Message = string.Empty;
    }

    public Result(TStatus status, TData data)
    {
        Status = status;
        Data = data;
        Message = string.Empty;
    }

    public Result(TStatus status, string message)
    {
        Status = status;
        Message = message;
        Data = default;
    }
}
