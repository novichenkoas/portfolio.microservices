﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Models;

public record ErrorModel(string PropertyName, string Error);
