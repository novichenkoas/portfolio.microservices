﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Enums;

public enum UserRole
{
	Admin,
	Manager,
	Specialist,
	Support,
	Client,
}
