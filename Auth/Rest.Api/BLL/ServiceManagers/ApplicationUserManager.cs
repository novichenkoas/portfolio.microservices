﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ServiceManagers;

public class ApplicationUserManager: UserManager<ApplicationUser>
{
    public ApplicationUserManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators,
        IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, 
        IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) 
        : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer,
            errors, services, logger)
    {
    }

    public virtual async Task<ApplicationUser?> FindByPhoneAsync(string phone) =>
        await Users.FirstOrDefaultAsync(e => e.PhoneNumber == phone);
}