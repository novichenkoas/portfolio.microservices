﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity.Enums;

public enum GetVerificationCodeStatus
{
    Success,
    GeneralError
}