﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Base;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services.Interfaces;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity.Handlers;

public class GetVerificationCodeQueryHandler: BaseQueryHandler<GetVerificationCodeQuery, GetVerificationCodeStatus, int>
{
    private readonly ICodeGenerator _codeGenerator;

    public GetVerificationCodeQueryHandler(ICodeGenerator codeGenerator) =>
        _codeGenerator = codeGenerator;

    protected override async Task<Result<GetVerificationCodeStatus, int>> ExecutingHandle(
        GetVerificationCodeQuery request, CancellationToken cancellationToken)
    {
        int fromInclusive = (int)Math.Pow(10, ApplicationConstants.VerificationPhoneCodeLength - 1);
        int code = _codeGenerator.GetInt32(fromInclusive, (fromInclusive * 10) - 1);
        return await Task.FromResult(new Result<GetVerificationCodeStatus, int>(
            GetVerificationCodeStatus.Success, code));
    }
}