﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity;

public class GetVerificationCodeQuery: IQuery<Result<GetVerificationCodeStatus, int>>
{
    
}