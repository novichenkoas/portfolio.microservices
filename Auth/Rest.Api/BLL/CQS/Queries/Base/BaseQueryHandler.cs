﻿using MediatR;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Base;

public abstract class BaseQueryHandler<TQuery, TStatus, TData>
    : IQueryHandler<TQuery, Result<TStatus, TData>>
    where TQuery : IRequest<Result<TStatus, TData>>
    where TStatus : Enum
{
    public async Task<Result<TStatus, TData>> Handle(TQuery request, CancellationToken cancellationToken)
    {
        try
        {
            return await ExecutingHandle(request, cancellationToken);
        }
        catch(Exception e)
        {
            return new Result<TStatus, TData>(
                (TStatus)Enum.Parse(typeof(TStatus), ApplicationConstants.GeneralError), e.Message);
        }
    }

    protected abstract Task<Result<TStatus, TData>>
        ExecutingHandle(TQuery request, CancellationToken cancellationToken);
}
