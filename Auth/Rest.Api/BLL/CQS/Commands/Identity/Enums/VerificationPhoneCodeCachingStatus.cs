﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity.Enums;

public enum VerificationPhoneCodeCachingStatus
{
    Success,
    NotEnoughTimeHasElapsedToUpdateTheCache,
    UserWithThisPhoneIsAlreadyRegistered,
    GeneralError,
    UserWithThisPhoneNotFound
}