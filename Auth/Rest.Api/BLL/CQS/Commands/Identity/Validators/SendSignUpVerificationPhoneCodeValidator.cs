﻿using FluentValidation;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Extensions;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity.Validators;

public class SendSignUpVerificationPhoneCodeValidator : AbstractValidator<SendSignUpVerificationPhoneCodeCommand>
{
    public SendSignUpVerificationPhoneCodeValidator()
    {
        RuleFor(x => x.Phone).AsPhoneCustomRule();
    }
}