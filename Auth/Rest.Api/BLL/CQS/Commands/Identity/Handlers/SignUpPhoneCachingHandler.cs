﻿using AutoMapper;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Base;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ServiceManagers;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity.Handlers;

public class SignUpPhoneCachingHandler: BaseCommandHandler<SignUpPhoneCachingCommand, VerificationPhoneCodeCachingStatus, object>
{
    private readonly IMapper _mapper;
    private readonly ApplicationUserManager _userManager;
    private readonly IVerificationCodeRepository<SignUpPhoneCode> _repository;

    public SignUpPhoneCachingHandler(IMapper mapper, ApplicationUserManager userManager,
        IVerificationCodeRepository<SignUpPhoneCode> repository) =>
        (_mapper, _userManager, _repository) = (mapper, userManager, repository);

    protected override async Task<Result<VerificationPhoneCodeCachingStatus, object>> ExecutingHandle(
        SignUpPhoneCachingCommand request, CancellationToken cancellationToken)
    {
        ApplicationUser dbUser = await _userManager.FindByPhoneAsync(request.Phone);
        if(dbUser != null)
        {
            return new Result<VerificationPhoneCodeCachingStatus, object>(
                VerificationPhoneCodeCachingStatus.UserWithThisPhoneIsAlreadyRegistered);
        }

        DatabaseResult<SignUpPhoneCode> dbResultSignUpPhoneCode =
            await _repository.GetAsync(request.Phone, cancellationToken);

        switch(dbResultSignUpPhoneCode.Status)
        {
            case DatabaseStatus.DbError or DatabaseStatus.GeneralError:
                return await Task.FromResult(new Result<VerificationPhoneCodeCachingStatus, object>(
                    VerificationPhoneCodeCachingStatus.GeneralError));

            case DatabaseStatus.Success when(DateTime.UtcNow - dbResultSignUpPhoneCode.Data.Created) <
                                             TimeSpan.FromMinutes(ApplicationConstants.ResendPhoneCodeTimespanMinutes):
                return await Task.FromResult(new Result<VerificationPhoneCodeCachingStatus, object>(
                    VerificationPhoneCodeCachingStatus.NotEnoughTimeHasElapsedToUpdateTheCache));

            case DatabaseStatus.Success:
                dbResultSignUpPhoneCode.Data.Canceled = true;
                DatabaseResult<long> updateResult =
                    await _repository.UpdateAsync(dbResultSignUpPhoneCode.Data, cancellationToken);
                if(updateResult.Status is DatabaseStatus.DbError or DatabaseStatus.GeneralError)
                {
                    return await Task.FromResult(new Result<VerificationPhoneCodeCachingStatus, object>(
                        VerificationPhoneCodeCachingStatus.GeneralError));
                }

                break;
        }

        DatabaseResult<long> createResult =
            await _repository.CreateAsync(_mapper.Map<SignUpPhoneCode>(request), cancellationToken);
        if(createResult.Status is DatabaseStatus.DbError or DatabaseStatus.GeneralError)
        {
            return await Task.FromResult(new Result<VerificationPhoneCodeCachingStatus, object>(
                VerificationPhoneCodeCachingStatus.GeneralError));
        }

        return await Task.FromResult(
            new Result<VerificationPhoneCodeCachingStatus, object>(VerificationPhoneCodeCachingStatus.Success));
    }
}