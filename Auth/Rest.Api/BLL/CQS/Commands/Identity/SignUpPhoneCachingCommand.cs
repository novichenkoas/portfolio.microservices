﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity;

public class SignUpPhoneCachingCommand: ICommand<Result<VerificationPhoneCodeCachingStatus, object>>
{
    public string Phone { get; set; }
    public int Code { get; set; }
}