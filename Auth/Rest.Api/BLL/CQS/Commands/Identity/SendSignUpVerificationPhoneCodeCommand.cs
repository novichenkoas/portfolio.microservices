﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels.ResultDataModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity;

public class SendSignUpVerificationPhoneCodeCommand
{
    public string? Phone { get; set; }
}