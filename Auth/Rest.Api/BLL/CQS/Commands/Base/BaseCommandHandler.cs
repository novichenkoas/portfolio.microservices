﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Base;

public abstract class BaseCommandHandler<TCommand, TStatus, TData>
	: ICommandHandler<TCommand, Result<TStatus, TData>>
	where TCommand : ICommand<Result<TStatus, TData>>
	where TStatus : Enum
{
	public async Task<Result<TStatus, TData>> Handle(TCommand request, CancellationToken cancellationToken)
	{
		try
		{
			return await ExecutingHandle(request, cancellationToken);
		}
		catch (Exception e)
		{
			return new Result<TStatus, TData>(
				(TStatus)Enum.Parse(typeof(TStatus), ApplicationConstants.GeneralError), e.Message);
		}
	}

	protected abstract Task<Result<TStatus, TData>> ExecutingHandle(TCommand request,
		CancellationToken cancellationToken);
}
