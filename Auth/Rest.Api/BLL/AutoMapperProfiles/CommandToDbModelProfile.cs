﻿using AutoMapper;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.AutoMapperProfiles;

public class CommandToDbModelProfile: Profile
{
    public CommandToDbModelProfile()
    {
        CreateMap<SignUpPhoneCachingCommand, SignUpPhoneCode>()
            .ForMember(code => code.Created, command => command.MapFrom(_ => DateTime.UtcNow))
            .ForMember(code => code.Expire, command => command.MapFrom(_ =>
                DateTime.UtcNow.AddMinutes(ApplicationConstants.CachingPhoneCodeTimespanMinutes)));
    }
}