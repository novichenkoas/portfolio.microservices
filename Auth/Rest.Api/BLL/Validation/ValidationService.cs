﻿using FluentValidation;
using FluentValidation.Results;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Models;
using Serilog;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Validation;

public class ValidationService : IValidationService
{
	private readonly IServiceProvider serviceProvider;
	private readonly ILogger logger;

	public ValidationService(IServiceProvider serviceProvider, ILogger logger)
	{
		this.serviceProvider = serviceProvider;
		this.logger = logger;
	}

	public async Task<ValidationServiceResult> ValidateAsync<T>(T model)
	{
		var validator = (IValidator<T>)serviceProvider.GetService(typeof(IValidator<T>));
		if (validator == null)
		{
			logger.Error("Not found validator '{name}'",
				typeof(IValidator<T>).AssemblyQualifiedName);

			return new ValidationServiceResult(false);
		}

		ValidationResult result = await validator.ValidateAsync(model);

		return new ValidationServiceResult(result.IsValid)
		{
			Errors =
				result.IsValid ? null : result.Errors.ConvertAll(x => new ErrorModel(x.PropertyName, x.ErrorMessage))
		};
	}
}
