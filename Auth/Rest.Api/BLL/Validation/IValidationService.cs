﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Validation;

public interface IValidationService
{
	Task<ValidationServiceResult> ValidateAsync<T>(T model);
}
