﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Models;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Validation;

public class ValidationServiceResult
{
	public bool IsValid { get; set; }

	public List<ErrorModel> Errors { get; set; }

	public ValidationServiceResult(bool isValid)
	{
		IsValid = isValid;
		Errors = null;
	}
}
