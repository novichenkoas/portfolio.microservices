﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services.Interfaces;

public interface ICodeGenerator
{
    int GetInt32(int fromInclusive, int toExclusive);
}