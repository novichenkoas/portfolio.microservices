﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services.Interfaces;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services
{
    public class MockCodeGeneratorService : ICodeGenerator
    {
        public int GetInt32(int fromInclusive, int toExclusive) => 111111;
    }
}
