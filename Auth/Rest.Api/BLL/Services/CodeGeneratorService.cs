﻿using System.Security.Cryptography;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services.Interfaces;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services;

public class CodeGeneratorService: ICodeGenerator
{
    public int GetInt32(int fromInclusive, int toExclusive) =>
        RandomNumberGenerator.GetInt32(fromInclusive, toExclusive);
}