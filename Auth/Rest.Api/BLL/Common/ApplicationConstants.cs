using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Enums;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Common;

public static class ApplicationConstants
{
	public const string EmailPattern =
		@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";

	public const string PhonePattern = @"^79\d{9}$";
	public const string VerificationPhoneCodePattern = @"^\d{6}$";
	public const int CachingPhoneCodeTimespanMinutes = 5;
	public const int CachingEmailCodeTimespanMinutes = 5;
	public const int ResendPhoneCodeTimespanMinutes = 1;
	public const int ResendEmailCodeTimespanMinutes = 1;
	public const int VerificationPhoneCodeLength = 6;
	public const int EmailMaxLengthLength = 50;
	public const int GetUsersQueryPageMaxLength = 100;
	public const int GetUsersQuerySearchEmailMaxLength = 256;
	public const int GetUsersQueryPhoneMaxLength = 11;
	public const string GeneralError = "GeneralError";

	public static readonly IEnumerable<string> ClientRoles =
		new[] { nameof(UserRole.Client), nameof(UserRole.Specialist) };

	public static readonly IEnumerable<string> EmployeeRoles =
		new[] { nameof(UserRole.Admin), nameof(UserRole.Manager), nameof(UserRole.Support) };
}
