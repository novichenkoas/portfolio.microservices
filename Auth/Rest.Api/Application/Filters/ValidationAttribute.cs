﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Response;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels.ResultDataModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Validation;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Filters
{
	[AttributeUsage(AttributeTargets.All)]
	public class ValidationAttribute : Attribute, IAsyncActionFilter
	{
		private readonly string _modelVariableName;

		public ValidationAttribute(string modelVariableName = "model")
		{
			_modelVariableName = modelVariableName;
		}

		public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
		{
			IValidationService validationService = (IValidationService)context.HttpContext.RequestServices.GetService(typeof(IValidationService));
			Serilog.ILogger logger = (Serilog.ILogger)context.HttpContext.RequestServices.GetService(typeof(Serilog.ILogger));
			dynamic result = context.ActionArguments[_modelVariableName];

			ValidationServiceResult validationResult = await validationService?.ValidateAsync(result)!;

			if (!validationResult.IsValid)
			{
				string errorLogResult = string.Join("; ", validationResult.Errors);
				logger?.Warning($"[{context.ActionDescriptor.DisplayName}] Invalid model: {errorLogResult}");
				context.Result =
					new BadRequestObjectResult(new HttpBaseResponse<UserResultDataModel>(validationResult.Errors));
				return;
			}

			await next();
		}
	}
}
