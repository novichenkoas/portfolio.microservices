﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Options;

public class JwtOptions
{
    public static string Section
    {
        get => "JwtOptions";
    }
    public string AccessTokenCookieName { get; set; }
    public string RefreshTokenCookieName { get; set; }
    public int AccessTokenCookieExpiration { get; set; }
    public int RefreshTokenCookieExpiration { get; set; }
    public string ValidTypes { get; set; }
    public string ValidIssuer { get; set; }
    public string ClientId { get; set; }
}