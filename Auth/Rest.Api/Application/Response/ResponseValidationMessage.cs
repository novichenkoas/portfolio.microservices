﻿namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Response;

public class ResponseValidationMessage
{
	public string Path { get; }
	public string Error { get; }

	public ResponseValidationMessage(string path, string error)
	{
		Path = path;
		Error = error;
	}
}
