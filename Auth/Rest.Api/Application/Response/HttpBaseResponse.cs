using System.Collections.ObjectModel;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Models;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Response;

public class HttpBaseResponse<TData>
{
	[System.Text.Json.Serialization.JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
	public TData Result { get; }

	[System.Text.Json.Serialization.JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
	public IReadOnlyList<ResponseValidationMessage> Validation { get; }

	[System.Text.Json.Serialization.JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
	public IReadOnlyList<Enum> Errors { get; }

	public HttpBaseResponse(TData data)
	{
		Result = data;
	}

	public HttpBaseResponse(params Enum[] errors)
	{
		Errors = errors;
	}

	public HttpBaseResponse(List<ErrorModel> errors)
	{
		Validation = new List<ResponseValidationMessage>(
			errors.Select(e => new ResponseValidationMessage(e.PropertyName, e.Error)));
	}

	public override string ToString() =>
		JsonConvert.SerializeObject(this,
			Formatting.None,
			new JsonSerializerSettings {
				NullValueHandling = NullValueHandling.Ignore,
				Converters = new Collection<Newtonsoft.Json.JsonConverter> { new StringEnumConverter() }
			});
}
