﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Response;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels.ResultDataModels;
using ILogger = Serilog.ILogger;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Controllers;

public class BaseApiController: ControllerBase
{
    protected readonly ILogger _logger;
    protected readonly IMediator _mediator;

    protected string CurrentPath
    {
        get => $"[{HttpContext?.Request?.Method} {HttpContext?.Request?.Path.ToString()}]";
    }

    protected BaseApiController(ILogger logger, IMediator mediator)
        => (_logger, _mediator) = (logger, mediator);

    protected IActionResult NotImplementResponse()
    {
        _logger.Error($"{CurrentPath} Not implement for return value");
        return StatusCode(StatusCodes.Status501NotImplemented);
    }

    protected IActionResult InternalServerErrorResponse(string message)
    {
        _logger.Error($"{CurrentPath} {message}");
        return StatusCode(StatusCodes.Status500InternalServerError,
            new HttpBaseResponse<UserResultDataModel>(MessageKey.InternalServerError));
    }
}