﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Response;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels.ResultDataModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Filters;
using Microsoft.AspNetCore.Authorization;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Queries.Identity.Enums;
using ILogger = Serilog.ILogger;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Controllers;

[AllowAnonymous, Route("api/auth/v{version:apiVersion}/sign-up")]
public class SignUpController: BaseApiController
{
    protected SignUpController(ILogger logger, IMediator mediator) : base(logger, mediator)
    {
    }

    [HttpPost("phone"), Validation]
    public async Task<IActionResult> Phone(SendSignUpVerificationPhoneCodeCommand model)
    {
        Result<GetVerificationCodeStatus, int> codeGenerationResult =
            await _mediator.Send(new GetVerificationCodeQuery());
        if(codeGenerationResult.Status == GetVerificationCodeStatus.Success)
        {
            SignUpPhoneCachingCommand dataCacheCommand =
                new SignUpPhoneCachingCommand { Code = codeGenerationResult.Data, Phone = model.Phone };

            Result<VerificationPhoneCodeCachingStatus, object> cachingResult =
                await _mediator.Send(dataCacheCommand);
            switch(cachingResult.Status)
            {
                case VerificationPhoneCodeCachingStatus.Success:
                    _logger.Debug($"{CurrentPath} Generated phone code");
                    return Ok();

                case VerificationPhoneCodeCachingStatus.UserWithThisPhoneIsAlreadyRegistered:
                    _logger.Error($"{CurrentPath} {cachingResult.Message}");
                    return Conflict(new HttpBaseResponse<UserResultDataModel>(MessageKey.AuthPhoneIsAlreadyExist));

                case VerificationPhoneCodeCachingStatus.NotEnoughTimeHasElapsedToUpdateTheCache:
                    _logger.Error($"{CurrentPath} Too many requests phone code by sign up");
                    return StatusCode(StatusCodes.Status429TooManyRequests,
                        new HttpBaseResponse<UserResultDataModel>(MessageKey.AuthTooManyRequestsCode));

                case VerificationPhoneCodeCachingStatus.GeneralError:
                    return InternalServerErrorResponse(cachingResult.Message);

                default:
                    return NotImplementResponse();
            }
        }

        return InternalServerErrorResponse(codeGenerationResult.Message);
    }

    /*
    [HttpPost("verify-phone"), Validation]
    public async Task<IActionResult> VerifyPhone(SignUpVerificationPhoneCommand model)
    {
        Result<VerificationStatus, SignUpPhoneCode> verificationResult =
            await _mediator.Send(model);

        switch(verificationResult.Status)
        {
            case VerificationStatus.Success:
                var createModel = new CreateUserByPhoneCommand { Phone = model.Phone, Role = model.Role };
                Result<CreateUserCommandStatus, UserResultDataModel> result = await _mediator.Send(createModel);

                switch(result.Status)
                {
                    case CreateUserCommandStatus.Success:
                        _logger.Information($"{CurrentPath} User created");
                        return StatusCode(StatusCodes.Status201Created,
                            new HttpBaseResponse<UserResultDataModel>(result.Data));

                    case CreateUserCommandStatus.LoginIsAlreadyExist:
                        _logger.Debug($"{CurrentPath} phone is already exist");
                        return Conflict(new HttpBaseResponse<UserResultDataModel>(MessageKey.AuthPhoneIsAlreadyExist));

                    case CreateUserCommandStatus.RegistrationError:
                        _logger.Error($"{CurrentPath} registration error");
                        return StatusCode(StatusCodes.Status500InternalServerError,
                            new HttpBaseResponse<UserResultDataModel>(MessageKey.AuthRegistrationError));

                    case CreateUserCommandStatus.GeneralError:
                        return InternalServerErrorResponse(result.Message);

                    default:
                        return NotImplementResponse();
                }

            case VerificationStatus.VerificationError:
                _logger.Error($"{CurrentPath} verification error");
                return NotFound(
                    new HttpBaseResponse<UserResultDataModel>(MessageKey.AuthNotFoundVerificationPhone));

            case VerificationStatus.InvalidVerificationCodeError:
                _logger.Debug($"{CurrentPath} invalid verification code");
                return BadRequest(
                    new HttpBaseResponse<UserResultDataModel>(MessageKey.AuthInvalidVerificationPhoneCode));

            case VerificationStatus.GeneralError:
                return InternalServerErrorResponse(verificationResult.Message);

            default:
                return NotImplementResponse();
        }
    }*/
}