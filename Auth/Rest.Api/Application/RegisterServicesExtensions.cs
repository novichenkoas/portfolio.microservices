﻿using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Services;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Services.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ServiceManagers;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Services;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Validation;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application;

public static class RegisterServicesExtensions
{
    public static void RegisterServices(
        this IServiceCollection serviceCollection,
        Serilog.ILogger logger)
    {
        string? environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        bool isDevelopment =
            string.Equals(environment, Environments.Development, StringComparison.InvariantCultureIgnoreCase);

        if(isDevelopment)
        {
            logger.Debug(
                $"Register '{typeof(MockCodeGeneratorService).AssemblyQualifiedName}' as '{nameof(ICodeGenerator)}'");
            serviceCollection.AddScoped<ICodeGenerator, MockCodeGeneratorService>();
        }
        else
        {
            logger.Debug(
                $"Register '{typeof(CodeGeneratorService).AssemblyQualifiedName}' as '{nameof(ICodeGenerator)}'");
            serviceCollection.AddScoped<ICodeGenerator, CodeGeneratorService>();
        }

        // Register validation service
        serviceCollection.AddScoped<IValidationService, ValidationService>();

        serviceCollection.AddScoped<ApplicationUserManager, ApplicationUserManager>();

        serviceCollection.AddScoped<ITokenService, TokenService>();
    }

    public static string GetDuendeIdentityServerLicenseKey()
    {
        string key =
            Environment.GetEnvironmentVariable("IDENTITY_SERVER_LICENSE_KEY", EnvironmentVariableTarget.Machine);

        return !string.IsNullOrEmpty(key) ? key : DuendeIdentityServerLicenseKey;
    }

    private const string DuendeIdentityServerLicenseKey = "";
}