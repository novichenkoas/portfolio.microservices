﻿using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Response;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Enums;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.ResultModels.ResultDataModels;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Extensions
{
	public static class ExceptionMiddlewareExtensions
	{
		public static void ConfigureExceptionHandler(this IApplicationBuilder app, Serilog.ILogger logger)
		{
			app.UseExceptionHandler(appError =>
			{
				appError.Run(async context =>
				{
					context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					context.Response.ContentType = "application/json";
					IExceptionHandlerFeature contextFeature = context.Features.Get<IExceptionHandlerFeature>();
					if (contextFeature != null)
					{
						logger.Error(contextFeature.Error.Message);
						await context.Response.WriteAsync(
							new HttpBaseResponse<UserResultDataModel>(MessageKey.InternalServerError)
								.ToString());
					}
				});
			});
		}
	}
}
