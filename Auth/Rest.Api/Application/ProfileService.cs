﻿using Duende.IdentityServer.Extensions;
using Duende.IdentityServer.Models;
using Duende.IdentityServer.Services;
using IdentityModel;
using Microsoft.AspNetCore.Identity;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;
using System.Security.Claims;
using ILogger = Serilog.ILogger;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application;

public class ProfileService: IProfileService
{
    private readonly IUserClaimsPrincipalFactory<ApplicationUser> _userClaimsPrincipalFactory;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly ILogger _logger;
    private readonly RoleManager<ApplicationRole> _roleManager;

    public ProfileService(UserManager<ApplicationUser> userManager, ILogger logger,
        RoleManager<ApplicationRole> roleManager,
        IUserClaimsPrincipalFactory<ApplicationUser> userClaimsPrincipalFactory) =>
        (_userManager, _logger, _roleManager, _userClaimsPrincipalFactory) =
        (userManager, logger, roleManager, userClaimsPrincipalFactory);

    public async Task GetProfileDataAsync(ProfileDataRequestContext context)
    {
        string sub = context.Subject.GetSubjectId();
        ApplicationUser user = await _userManager.FindByIdAsync(sub);
        ClaimsPrincipal userClaims = await _userClaimsPrincipalFactory.CreateAsync(user);

        List<Claim> claims = userClaims.Claims.ToList();
        claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();

        if(_userManager.SupportsUserRole)
        {
            IList<string> roles = await _userManager.GetRolesAsync(user);
            foreach(var roleName in roles)
            {
                claims.Add(new Claim(JwtClaimTypes.Role, roleName));
                if(_roleManager.SupportsRoleClaims)
                {
                    ApplicationRole role = await _roleManager.FindByNameAsync(roleName);
                    if(role != null)
                    {
                        claims.AddRange(await _roleManager.GetClaimsAsync(role));
                    }
                }
            }
        }

        context.IssuedClaims.AddRange(claims);

    }

    public async Task IsActiveAsync(IsActiveContext context)
    {
        ApplicationUser user = await _userManager.GetUserAsync(context.Subject);
        if(user == null)
        {
            _logger.Information($"User not found matching to: {context.Subject.Identity?.Name} ");
        }

        context.IsActive = user != null;
    }
}