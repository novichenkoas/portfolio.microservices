﻿using System.Security.Claims;
using Duende.IdentityServer;
using Microsoft.Extensions.Options;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Options;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Services.Interfaces;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Services;

public class TokenService : ITokenService
{
	private readonly IdentityServerTools _identityServerTools;
	private readonly IHttpContextAccessor _httpContextAccessor;
	private readonly JwtOptions _jwtOptions;

	public TokenService(
		IdentityServerTools identityServerTools,
		IHttpContextAccessor httpContextAccessor,
		IOptions<JwtOptions> jwtOptions)
	{
		_identityServerTools = identityServerTools;
		_httpContextAccessor = httpContextAccessor;
		_jwtOptions = jwtOptions.Value;
	}

	public async Task SetSigninToken(IEnumerable<Claim> claims)
	{
		string token = await _identityServerTools
			.IssueClientJwtAsync(_jwtOptions.ClientId,
								 _jwtOptions.AccessTokenCookieExpiration,
								 additionalClaims: claims);
		_httpContextAccessor.HttpContext?
			.Response
			.Cookies
			.Append(_jwtOptions.AccessTokenCookieName,
				token,
				new CookieOptions {
					Expires = new DateTimeOffset(DateTime.UtcNow
						.AddSeconds(_jwtOptions.AccessTokenCookieExpiration)),
					HttpOnly = true,
					Secure = true,
					SameSite = SameSiteMode.None,
					IsEssential = true,
				});
	}

	public void RemoveSigninToken()
	{
		_httpContextAccessor.HttpContext?.Response.Cookies.Delete(_jwtOptions.AccessTokenCookieName);
	}
}
