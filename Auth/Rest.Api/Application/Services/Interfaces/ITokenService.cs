﻿using System.Security.Claims;

namespace NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Services.Interfaces;

public interface ITokenService
{
    Task SetSigninToken(IEnumerable<Claim> claims);
    void RemoveSigninToken();
}
