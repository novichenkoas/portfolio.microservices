using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Text.Json.Serialization;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbContext;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.DbModels;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Options;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.AutoMapperProfiles;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.CQS.Commands.Identity;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Interfaces;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Dal.Repositories;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Bll.Validation;
using Duende.IdentityServer;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using NovichenkoSoft.Portfolio.Microservices.Auth.Rest.Api.Application.Extensions;

var builder = WebApplication.CreateBuilder(args);

IConfigurationRoot configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json")
    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json",
        true)
    .AddEnvironmentVariables()
    .Build();

string dbName = configuration["Auth_DB_Database"];
string dbUserName = configuration["Auth_DB_User"];
string dbUserPassword = configuration["Auth_DB_Pass"];
string dbHost = configuration["Auth_DB_Host"];
string dbHostPort = configuration["Auth_DB_Port"] ?? "5432";

string connectionString = $"Server={dbHost};Port={dbHostPort};Database={dbName};" +
                          $"Userid={dbUserName};Password={dbUserPassword};";

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration).WriteTo.Console()
    .CreateLogger();

// Register logger
builder.Services.AddSingleton<Serilog.ILogger>(Log.Logger);

// Add httpContext
builder.Services.AddHttpContextAccessor();

// Add services to the container.
builder.Services.AddControllers()
    .AddJsonOptions(options => { options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()); })
    .AddFluentValidation(
        options =>
        {
            options.AutomaticValidationEnabled = false;
            options.RegisterValidatorsFromAssembly(typeof(ICommand<>).Assembly);
        });

// API Version
builder.Services.AddApiVersioning(opt =>
{
    opt.DefaultApiVersion = new ApiVersion(2, 0);
    opt.AssumeDefaultVersionWhenUnspecified = true;
    opt.ReportApiVersions = true;
});

//identity
builder.Services.AddDbContext<MainContext>(config =>
    {
        config.UseNpgsql(connectionString);
    })
    .AddIdentity<ApplicationUser, ApplicationRole>(config =>
    {
        config.Password.RequireDigit = true;
        config.Password.RequiredLength = 8;
        config.Password.RequireLowercase = true;
        config.Password.RequireUppercase = true;
        config.Password.RequireNonAlphanumeric = true;
    })
    .AddEntityFrameworkStores<MainContext>()
    .AddDefaultTokenProviders();

//identityServer configuration
JwtOptions jwtOptions = configuration.GetSection(JwtOptions.Section).Get<JwtOptions>();
builder.Services.AddIdentityServer(options =>
    {
        //options.LicenseKey = RegisterServicesExtensions.GetDuendeIdentityServerLicenseKey();
        //options.Authentication.CheckSessionCookieName = configuration["DuendeIdentityServerAuthCookie"];
        options.Authentication.CookieSameSiteMode = SameSiteMode.None;
        options.Authentication.CheckSessionCookieSameSiteMode = SameSiteMode.None;
        options.KeyManagement.Enabled = false;
        options.IssuerUri = jwtOptions.ValidIssuer;
        options.AccessTokenJwtType = jwtOptions.ValidTypes;
    })
    .AddJwtBearerClientAuthentication()
    .AddAspNetIdentity<ApplicationUser>()
    .AddOperationalStore(options =>
    {
        options.ConfigureDbContext = b => b.UseNpgsql(connectionString);
    })
    .AddConfigurationStore(options =>
    {
        options.ConfigureDbContext = b => b.UseNpgsql(connectionString);
    })
    .AddProfileService<ProfileService>()
    .AddDeveloperSigningCredential();
//.AddSigningCredential(PriemX509Certificate.Certtificate);

builder.Services.AddEndpointsApiExplorer();


//TODO: Implement normal health check functionality
builder.Services.AddHealthChecks()
    .AddCheck("default", () => HealthCheckResult.Healthy("platform.auth.rest.api state is healthy"));

//authentication token settings
//builder.Services.AddPriemJwt(configuration);

builder.Services.ConfigureApplicationCookie(options =>
{
    options.Events.OnRedirectToLogin = context =>
    {
        context.Response.Clear();
        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
        return Task.CompletedTask;
    };
    options.Events.OnRedirectToAccessDenied = context =>
    {
        context.Response.Clear();
        context.Response.StatusCode = StatusCodes.Status403Forbidden;
        return Task.CompletedTask;
    };
});

builder.Services.AddMediatR(typeof(SignUpPhoneCachingCommand).Assembly);
builder.Services.AddAutoMapper(typeof(CommandToDbModelProfile).Assembly);

// Register Repositories
builder.Services
    .AddScoped<IVerificationCodeRepository<SignUpPhoneCode>, VerificationPhoneCodeRepository<SignUpPhoneCode>>();

// Register logger
builder.Services.AddSingleton(Log.Logger);

// Register DB context
builder.Services.AddScoped<DbContext, MainContext>();

// Register service provider
IServiceProvider serviceProvider = builder.Services.BuildServiceProvider();
builder.Services.AddSingleton(serviceProvider);

//Register services
builder.Services.RegisterServices(Log.Logger);

// Register validation service
builder.Services.AddScoped<IValidationService, ValidationService>();


// Register settings
builder.Services.Configure<JwtOptions>(
    builder.Configuration.GetSection(JwtOptions.Section));

builder.Services.Configure<CookieAuthenticationOptions>(IdentityServerConstants.DefaultCookieAuthenticationScheme, options =>
{
    options.Cookie.SameSite = SameSiteMode.None;
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.Cookie.IsEssential = true;
});


var app = builder.Build();

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

//global exception middleware
app.ConfigureExceptionHandler(Log.Logger);

app.UseCors(config => config
    .SetIsOriginAllowed(_ => true)
    .AllowCredentials()
    .AllowAnyMethod()
    .AllowAnyHeader()
);

app.UseAuthentication();

app.UseAuthorization();

app.UseIdentityServer();

app.UseApiVersioning();

app.MapControllers();

app.MapHealthChecks("/healthcheck");

app.Run();
